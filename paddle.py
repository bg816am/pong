from turtle import Turtle

PADDLE_SIZE_X = 1
PADDLE_SIZE_Y = 5
PADDLE_SPEED = 20


class Paddle(Turtle):
    def __init__(self, position):
        super().__init__()
        self.shape("square")
        self.shapesize(stretch_len=PADDLE_SIZE_X, stretch_wid=PADDLE_SIZE_Y)
        self.color("white")
        self.penup()
        self.goto(position)
        self.speed("fastest")

    def up(self):
        new_y = self.ycor() + PADDLE_SPEED
        self.goto(self.xcor(), new_y)

    def down(self):
        new_y = self.ycor() - PADDLE_SPEED
        self.goto(self.xcor(), new_y)
