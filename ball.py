from turtle import Turtle

BALL_SIZE_X = 1
BALL_SIZE_Y = 1
BOUNCE_FACTOR = 10


class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.color("white")
        self.penup()
        self.shapesize(stretch_len=BALL_SIZE_X, stretch_wid=BALL_SIZE_Y)
        self.x_move = BOUNCE_FACTOR
        self.y_move = BOUNCE_FACTOR

    def move_ball(self):
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto((new_x, new_y))

    def bounce_y(self):
        self.y_move *= -1

    def bounce_x(self):
        self.x_move *= -1

    def reset_position(self):
        self.goto(0, 0)

